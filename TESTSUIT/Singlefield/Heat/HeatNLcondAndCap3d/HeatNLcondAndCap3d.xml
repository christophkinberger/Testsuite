<?xml version="1.0"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>Heat conduction (NON LINEAR) - feasibility study 3D - TRANSIENT.</title>
        <authors>
            <author>Helmut Koeck, Manfred Kaltenbacher</author>
        </authors>
        <date>2011-08-30</date>
        <keywords>
            <keyword>lagrange multiplier</keyword>
        </keywords>
        <references>
            none
        </references>
        <isVerified>yes</isVerified>        
        <description>
            This example served as a test case while implementing 
            the heat conduction PDE. The model consists of 3 identical blocks (3D) with
            the source at the bottom, a possible non-linear layer inbetween (IMD) 
            and a top layer (PM). All regions are meshed with brick elements (linear).
            The analysis type now is TRANSIENT.
            
            The model and mesh was generated with the aid of ANSYS.
            model3DheatNL.in
            
            Update (07.12.2013): non-linear material properties (Silicon) implemented
            
        </description>
    </documentation>
       
    <fileFormats>
        <input>
            <hdf5 fileName="model3DheatNL.h5" id="smodel"/>
        </input>
        <output>
            <text id="1"/>
            <hdf5 id="hdf5"/>
        </output>   
        <materialData file="IRmat_NL.xml" format="xml"/>
    </fileFormats>
    
    <domain geometryType="3d">
        <regionList>
            <region name="Vol_EPI" material="Si8lin"/>
            <region name="Vol_IMD" material="Si8"/>
            <region name="Vol_PM" material="Cu1"/>
        </regionList>
        
        <surfRegionList>
        </surfRegionList>
        
        <nodeList>
            <nodes name="bc_top_manual">
                <list>
                    <freeCoord comp="x" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <freeCoord comp="z" start="0.0e-3" stop="1.00e-3" inc="0.1e-3"/>
                    <fixedCoord comp="y" value="3.0e-3"/>
                </list>
            </nodes>     
            <nodes name="saveT">
                <coord x="0.0005" y="0.001" z="0.001"/>
            </nodes>   
        </nodeList>
    </domain>
    
    <!-- Preliminary STATIC analysis step required in order to initialize nodes to Ta (298K) -->
    <sequenceStep index="1">
        <analysis>
            <static></static>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="Vol_EPI"/>
                    <region name="Vol_IMD"/>
                    <region name="Vol_PM"/>
                </regionList>
                <bcsAndLoads>
                    <temperature name="bc_top_manual" value="298.00"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </heatConduction>
        </pdeList>
        
    </sequenceStep>
       
    <sequenceStep index="2">
        <analysis>
            <transient>               
                <numSteps>
                   10
                </numSteps>
                <deltaT>
                    2E-2
                </deltaT>                
            </transient>            
        </analysis>
        
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="Vol_EPI"/>
                    <region name="Vol_IMD" nonLinIds="cond cap"/>
                    <region name="Vol_PM"/>
                </regionList>
                
            <nonLinList>
                  <heatConductivity id="cond"/>
                  <heatCapacity id="cap"/>
            </nonLinList>   

            <initialValues>
               <initialState>
                 <sequenceStep index="1"/>
              </initialState>
            </initialValues>
 
            <bcsAndLoads>                    
                <temperature name="bc_top_manual" value="298.00"/>
                <heatSourceDensity name="Vol_EPI" value="1e10"/>
            </bcsAndLoads>
                         
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                        <nodeList>
                          <nodes name="saveT" outputIds="1"/>
                        </nodeList>
                    </nodeResult>
                </storeResults>
            </heatConduction>
            
        </pdeList>

        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes">
                            <lineSearch type="none"/>
                            <incStopCrit> 1e-7</incStopCrit>
                            <resStopCrit> 1e-7</resStopCrit>
                            <maxNumIters> 200  </maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
            </system>
        </linearSystems>

    </sequenceStep>

</cfsSimulation>
