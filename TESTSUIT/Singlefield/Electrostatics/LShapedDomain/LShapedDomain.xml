<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  
  <documentation>
    <title>L-shaped domain</title>
    <authors>
      <author>ahauck</author>
    </authors>
    <date>2014-09-19</date>
    <keywords>
      <keyword>electrostatic</keyword>
      <keyword>p-FEM-Legendre</keyword>
    </keywords>
    <references>
      L. Demkowicz, Computing with hp-adaptive FEM,
      p.234f
      
      A. Hauck, PhD Higher Order Finite Elements for Coupled 
      and Anisotropic Field Problems, 2015
    </references>
    <isVerified>yes</isVerified>
    <description>
      This setup consists of a L-shaped domain, where the solution
      is prescribed on the complete boundary of the domain 
      ("manufactured solution"). The setup is of type "corner 
      singularity", which is ideally to demonstrate the convergence
      behavior of the p- and h-version of the FEM.      
      The corresponding ansys script can generate either regular or 
      graded meshes to prove convergence.
      
      On the interior boundary, the potential is set to ground. On the 
      outer boundary the electric flux according to the analyitical
      solution is prescribed.      
      
      The goal is to compare the total energy, which can be calculated
      semi-analytically to 0.918113330937582 Ws (see 
      python script analytical.py). The numerical value can be found in
      the history directory.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5/>
    </input>
    <output>	
      <hdf5 id="h5"/>
      <text id="txt" entityNumbering="consecutive"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane" printGridInfo="no">
    <regionList>
      <region name="domain" material="air"/>
    </regionList>
    <surfRegionList>
      <surfRegion name="bound"/>
    </surfRegionList>
  </domain>
  
  <fePolynomialList>
    <Legendre id="default">
      <isoOrder>5</isoOrder>
    </Legendre>
  </fePolynomialList>
  
  
  <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
    <pdeList>
      <electrostatic systemId="elec">
        <regionList>
          <region name="domain" polyId="default"/>
        </regionList>
        
        <bcsAndLoads>
          <!-- Specify Neumann term analytically on outer boundary -->          
          <fluxDensity name="bound">
            <comp dof="x" value="2.0/3.0*x*(x^2 + y^2)^(-2.0/3.0)*sin(2*atan2(y, x)/3 + pi/3) - 2*y*(x^2 + y^2)^(-2.0/3.0)*cos(2*atan2(y, x)/3 + pi/3)/3"/>
            <comp dof="y" value="2*x*(x^2 + y^2)^(-2.0/3.0)*cos(2*atan2(y, x)/3 + pi/3)/3 + 2.0/3.0*y*(x^2 + y^2)^(-2.0/3.0)*sin(2*atan2(y, x)/3 + pi/3)"/>
          </fluxDensity>
          <ground name="interior"/>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="elecPotential">
            <allRegions/>
          </nodeResult>
          <elemResult type="elecFieldIntensity">
            <allRegions/>
          </elemResult>
          <regionResult type="elecEnergy">
            <allRegions outputIds="txt,h5"/>
          </regionResult>
        </storeResults>
      </electrostatic>
    </pdeList>
    <linearSystems>
      <system id="elec">
        <solutionStrategy>
          <standard>
            <!-- Note: we use static condensation to increase solver performance -->
            <setup idbcHandling="elimination" staticCondensation="yes"/>
            <solver id="default"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
