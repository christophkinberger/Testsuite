<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">

  <documentation>
    <title>Current Carrying Conductor / Coil, nonlinear, 2level</title>
    <authors>
      <author>ahauck</author>
    </authors>
    <date>2012-02-26</date>
    <keywords>
      <keyword>magneticEdge</keyword>
      <keyword>nonlinear</keyword>
      <keyword>coil</keyword>
    </keywords>
    <references>
      n.A.
    </references>
    <!-- This is just a proof of concept simulation,
         to ensure working functionality.    -->
    <isVerified>no</isVerified>
    <description>
   This is a 3D model of a current carying rod, surrounded by
   a nonlinear material. We solve it using a two-step algothm,
   where initially the nonlinear problem is solved just on the 
   first order unknowns (Nedelec functions). After convergence,
   the full system is solvedd, with the first order solution as
   initial value.
   
   This siulation is merely a proof-of concept simulation to 
   verify the nonlinear algorithm.
    </description>
  </documentation>
  <fileFormats>
    
    <output>
    <hdf5/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region name="air" material="iron"/>
      <region name="coil" material="AIR"/>
    </regionList>
    <elemList>
      <elems name="bottom"/>
      <elems name="top"/>
      <elems name="surfxOuter"/>
      <elems name="surfyOuter"/>
    </elemList>
  </domain>
  
  <fePolynomialList>
    <Legendre id="default">
      <isoOrder>1</isoOrder>
    </Legendre>
  </fePolynomialList>
    
   <sequenceStep>
    <analysis>
      <static/>
    </analysis>
    
     <pdeList>
       <magneticEdge>
         <regionList>
           <!-- 1) Nonlinear section -->
           <region name="air" nonLinIds="p" polyId="default"/>
           
           <!-- 2) Linear section -->
           <!-- <region name="air" polyId="default" />-->
           
           <region name="coil" polyId="default"/>
         </regionList>
         
         <nonLinList>
           <permeability id="p"/>
         </nonLinList>
         
         <bcsAndLoads>
           <!-- B-tangential surfaces -  Dirichlet -->
           <fluxParallel name="botSurf"/>
           <fluxParallel name="topSurf"/>
           <fluxParallel name="xOuterSurf"/>
           <fluxParallel name="yOuterSurf"/>
           
           <!-- B normal surfaces - nothing to do -->
         </bcsAndLoads>
         
	 <coilList>
           <coil id="myCoil">
            <source type="current" value="4000"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
		<direction>
		   <analytic>
                    <comp dof="z" value="1"/>
                  </analytic>
		</direction>
                <wireCrossSection area="1"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
         
         <storeResults>          
           <elemResult type="magFluxDensity">
             <allRegions/>
           </elemResult>
           <!--<elemResult type="magElemPermeability">
             <allRegions/>
             </elemResult>
             <regionResult type="magEnergy"> 
            <allRegions/>
          </regionResult>-->
        </storeResults>
      </magneticEdge>
    </pdeList>
     <linearSystems>
       <system>
         <solutionStrategy>
             <!-- ==================== -->
             <!--  TWO LEVEL STRATEGY  --> 
             <!-- ==================== -->
             
             <!-- In this setup we pursue a two-level, two step approach:
               The system is split into two blocks and additional
               an inner block, which arises from static condensation.
               
                   (K_00 K_0F K_0I )
               K = (  #  K_FF K_FI )
                   (  #   #   K_II )
               
               K_00 .. block for lowest order Nedelec / edge elements
               K_FF .. block for face functions
               K_II .. block for interior functions (static condensation)
               
               The system is solved using a CG-solver, which gets preconditioned
               by a hybrid, block preconditioner:
               
                   ( K_00^-1                   )
               C = (         K*_FF^-1          )
                   (                  K*_II^-1 )
               
               K_00^-1  .. inverse of K_00 (direct solver)
               K*_FF^-1 .. block inverse of K_FF (= one block per face)
               K*_II^-1 .. block inverse of K_II 
                           (= static condensation, as inner blocks are
                            decoupled)
               
               For the nonlinear solution, we perform a two step approach:
                 1) Initially only the K_00 block is solved until convergence.
                 2) In a second step the whole (3x3) matrix is considered, where
                    the solution from step 1) is taken as initial value.                 
             -->
             <twoLevel>
               <setup calcConditionNumber="no" 
                 staticCondensation="yes" idbcHandling="elimination"/>
               <splitting>
                 <!-- Level 1: Lowest order Nedelec functions -->
                 <level num="1">
                   <matrix reordering="Metis" storage="sparseSym"/>
                 </level>
                 <!-- Level 2: Higher order functions (edges, faces) -->
                 <level num="2">
                   <matrix reordering="noReordering" storage="variableBlockRow"/>
                 </level>
               </splitting>
               <solution>
                 <!-- Two steps: -->
                 <step num="1" level="1">
                   <!-- In a first step, only the first level is considered (K_00) -->
                   <solver id="cg"/>
                   <precond id="sbm"/>
                   <nonLinear logging="yes" method="newton">
                     <lineSearch type="minEnergy"/>
                     <incStopCrit> 1e-2</incStopCrit>
                     <resStopCrit> 1e-2</resStopCrit>
                     <maxNumIters> 20  </maxNumIters>
                   </nonLinear>
                 </step>
                 <!-- In a second step, the complete system is solved -->
                 <step num="2" level="2">
                   <solver id="cg"/>
                   <precond id="sbm"/>
                   <nonLinear logging="yes" method="newton">
                     <lineSearch type="minEnergy"/>
                     <incStopCrit> 1e-2</incStopCrit>
                     <resStopCrit> 1e-2</resStopCrit>
                     <maxNumIters> 10 </maxNumIters>
                   </nonLinear>
                 </step>
               </solution>
             </twoLevel>
           <!--<standard>
             <nonLinear logging="yes" method="newton">
               <lineSearch type="minEnergy"/>
               <incStopCrit> 1e-2</incStopCrit>
               <resStopCrit> 1e-2</resStopCrit>
               <maxNumIters> 20  </maxNumIters>
             </nonLinear>
           </standard>-->
         </solutionStrategy>
         <solverList>
           <!-- We use a standard CG-solver for the SBM-system -->
           <cg id="cg">
             <tol>1e-10</tol>
             <maxIter>400</maxIter>
             <logging>yes</logging>
           </cg>
         </solverList>
         <precondList>
           <!-- Use a compound SBM diagonal preconditioner, which uses the
             following preconditioners for the single blocks:
             
             (1,1)-block: direct solver (= exact inverse)
             (2,2)-block: block Jacobian preconditioner (additive SCHWARZ method)
           -->
           <SBMDiag id="sbm">
             <precond block="1" id="dir"/>
             <precond block="2" id="bj"/>
           </SBMDiag>
           <!-- Preconditioner for (1,1)-block -->
           <directLDL id="dir"/>
           <!-- Preconditioner for (2,2)-block -->
           <BlockJacobi id="bj"/>
         </precondList>
       </system>
     </linearSystems>
   </sequenceStep>
    
</cfsSimulation>
