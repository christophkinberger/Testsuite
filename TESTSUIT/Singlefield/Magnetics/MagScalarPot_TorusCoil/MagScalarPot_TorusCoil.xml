<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation 
    file:/home/kroppert/Devel/CFS_SRC/latest_trunkGit/CFS/share/xml/CFS-Simulation/CFS.xsd">
    

    <documentation>
        <title>Nonlinear current driven torus coil simulated with magnetic scalar potential (Psi formulation) </title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2022-06-30</date>
        <keywords>
            <keyword>magneticScalar</keyword>
        </keywords>
        <references></references>
        <isVerified>no</isVerified>
        <description> A constant current density is impressed in the torus coil and the source magnetic field
                      is solved with a linear magnetic edge simulation (everything assumed to be linear with 
                      vacuum permeability, that's why there is a new flag in the magneticEdge, called onlyVacuum,
                      which is false by default. This source field is then used in the magnetic reduced scalar
                      potential formulation in sequence step 2 and here, the nonlinearity (mu(H), instead of nu(B)
                      in A-based formulations) is used.
                      </description>
    </documentation>

    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <hdf5 fileName="MagScalarPot_TorusCoil.h5ref" />
        </input>
        <output>
            <hdf5 id="hdf5"/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <domain geometryType="3d">
        <variableList>
            <var name="i" value="1"/>
            <var name="N" value="250"/>
            <var name="f_coil" value="10"/>
            <var name="A" value="0.002"/>
        </variableList>
        <regionList>
            <region name="V_core" material="iron_analytic" />
            <region name="V_coil" material="air"/>
            <region name="V_air" material="air"/>
        </regionList>
        <surfRegionList>
            <surfRegion name="S_z"/>
            <surfRegion name="S_y"/>
            <surfRegion name="S_x"/>
        </surfRegionList>
        <nodeList>
            <nodes name="N_gauge"/>
        </nodeList>
        <coordSysList>
            <cylindric id="cyl1">
                <origin x="0.0" y="0.0" z="0.0"/>
                <zAxis x="0" y="0" z="1"/>
                <rAxis x="1" y="0" z="0"/>
            </cylindric>
        </coordSysList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder> 0 </isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>

    <sequenceStep index="1">
        <analysis>
            <static></static>
        </analysis>

        <pdeList>
            <magneticEdge onlyVacuum="true" >
                <regionList>
                    <region name="V_core" polyId="Hcurl"/>
                    <region name="V_coil" polyId="Hcurl"/>
                    <region name="V_air" polyId="Hcurl"/>
                </regionList>

                <bcsAndLoads>
                    <fluxParallel name="S_z"/>
                    <fluxParallel name="S_y"/>
                    <fluxParallel name="S_x"/>
                    <fluxParallel name="S_outer"/>
                </bcsAndLoads>

                <coilList>
                    <coil id="coil1">
                        <source type="current" value="i*N"/>
                        <part id="1">
                            <regionList>
                                <region name="V_coil"/>
                            </regionList>
                            <direction>
                                <analytic coordSysId="cyl1">
                                    <comp dof="phi" value="1.0"/>
                                </analytic>
                            </direction>
                            <wireCrossSection area="A"/>
                        </part>
                    </coil>
                </coilList>
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions outputIds="hdf5"/>
                    </elemResult>
                    <elemResult type="magFieldIntensity">
                        <allRegions outputIds="hdf5"/>
                    </elemResult>
                </storeResults>
            </magneticEdge>
        </pdeList>
    </sequenceStep>




    <sequenceStep index="2">
        <analysis>
            <static></static>
        </analysis>

        <pdeList>
            <magnetic formulation="Psi">
                <regionList>
                    <region name="V_core" polyId="H1" nonLinIds="nl1"/>
                    <region name="V_coil" polyId="H1"/>
                    <region name="V_air" polyId="H1"/>
                </regionList>

                <nonLinList>
                    <permeability id="nl1" />
                </nonLinList>

                <bcsAndLoads>
                    <potential name="N_gauge" value="0"/>
                    <fieldIntensity name="V_air">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity  name="V_coil">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>

                    <fieldIntensity name="V_core">
                        <sequenceStep index="1">
                            <quantity name="magFieldIntensity" pdeName="magneticEdge" />
                            <timeFreqMapping>
                                <constant  step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </fieldIntensity>
                </bcsAndLoads>

                <storeResults>
                    
                    <elemResult type="magFluxDensity">
                        <allRegions />
                    </elemResult>
                    <!--<elemResult type="magElemPermeability">
                        <regionList>
                            <region name="V_core" />
                        </regionList>
                    </elemResult>-->
                </storeResults>
            </magnetic>
        </pdeList>

        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <nonLinear logging="yes" method="newton">
                            <lineSearch />
                            <incStopCrit>1e-2</incStopCrit>
                            <resStopCrit>1e-2</resStopCrit>
                            <maxNumIters>100</maxNumIters>
                        </nonLinear>
                    </standard>
                </solutionStrategy>
            </system>
        </linearSystems>
    </sequenceStep>


</cfsSimulation>
