<cfsSimulation xmlns="http://www.cfs++.org/simulation"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.cfs++.org/simulation ../../../../../share/xml/CFS-Simulation/CFS.xsd">
 
  <documentation>
    <title>Glass of Water</title>
    <authors>
      <author>ascharner</author>
    </authors>
    <date>2023-09-13</date>
    <keywords>
      <keyword>waterWaves</keyword>
    </keywords>
    <references>
    </references>
    <isVerified>no</isVerified>
    <description>
      Used for verification of the Force/Torque results.
      Test Setup:
      - Node at Container Bottom Middle (P_0): Fixed in y-direction, a horizontal force is applied in x-direction.
      - Node at Container Bottom Side (P_1): A high stiffness spring is attached in y-direction.
      - Resulting Force/Torque and correspondig Density Results on the Container-Wall Surface are stored.

      Postprocessing (done in Postprocessing.ipynb): 
      - Compare the applied force in x-direction to Force Result.
      - Compute the applied torque from the spring displacement at the container edge (M=F*r),
        and compare to Torque result.
    </description>
  </documentation>
  <fileFormats>
    <input>
      <cdb fileName="RectangularContainer.cdb"/>
    </input>
    <output>
      <hdf5/>
      <text id="txt" />
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="S_water" material="water"/>
      <region name="S_box" material="glass"/>
    </regionList>
  </domain> 
  
  <sequenceStep index="1">
    <analysis>
      <harmonic>
        <numFreq>75</numFreq>
        <startFreq>0.1
        </startFreq>
        <stopFreq>7.5</stopFreq>
      </harmonic>
    </analysis>
    <pdeList>
      <waterWave> 
        <regionList>
          <region name="S_water"/>
        </regionList>
        <bcsAndLoads>
          <freeSurfaceCondition name="L_surface" volumeRegion="S_water"/>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="waterPressure" complexFormat="realImag">
            <allRegions/>
          </nodeResult>
          <elemResult type="waterPosition">
            <allRegions/>
          </elemResult>	
          <surfElemResult type="waterSurfaceTraction">
            <surfRegionList>
              <surfRegion name="L_coupling"/>
            </surfRegionList>
          </surfElemResult>
          <surfElemResult type="waterSurfaceTorqueDensity">
            <surfRegionList>
              <surfRegion name="L_coupling"/>
            </surfRegionList>
          </surfElemResult>
          <surfRegionResult type="waterSurfaceForce">
            <surfRegionList>
              <surfRegion name="L_coupling" outputIds="txt"/>
            </surfRegionList>
          </surfRegionResult>
          <surfRegionResult type="waterSurfaceTorque">
            <surfRegionList>
              <surfRegion name="L_coupling" outputIds="txt"/>
            </surfRegionList>
          </surfRegionResult>
        </storeResults>
      </waterWave>
      <mechanic subType="planeStrain">
        <regionList>
          <region name="S_box" dampingId="myTanD"/>
        </regionList>
        <dampingList>
          <rayleigh id="myTanD">
            <adjustDamping>yes</adjustDamping>
          </rayleigh>
        </dampingList>
        <bcsAndLoads>
          <fix name="P_0">
            <comp dof="y"/>
          </fix>
          <concentratedElem name="P_1" dof="y" stiffnessValue="100000"/>
          <force name="P_0">
            <comp dof="x" value="10" />
          </force>
        </bcsAndLoads>
        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="P_0" outputIds="txt"/>
              <nodes name="P_1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="vonMisesStress">
            <allRegions/>
          </elemResult>
        </storeResults>
      </mechanic>
    </pdeList>
    <couplingList>
      <direct>
        <waterWaveMechDirect>
          <surfRegionList>
            <surfRegion name="L_coupling"/>
          </surfRegionList>
        </waterWaveMechDirect>
      </direct>
    </couplingList>
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym" reordering="noReordering"/>
          </standard>
        </solutionStrategy>
      </system>
   </linearSystems>
  </sequenceStep>
</cfsSimulation>