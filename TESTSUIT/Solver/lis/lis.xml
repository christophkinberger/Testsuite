<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>LIS iterative Solver</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2023-02-18</date>
    <keywords>
      <keyword>iterative_solver</keyword>
    </keywords>
    <references>https://www.ssisc.org/lis/index.en.html</references>
    <isVerified>yes</isVerified>
    <description>The lis iterative solver package https://www.ssisc.org/lis/index.en.html is a package of efficient iterative solvers.
      It has a wide range of preconditioners and solvers. The range of performance and stability is hughe. E.g. algeraic multigrid 
      preconditioner is very fast but fails ill conditioned systems. Some combinations have issues with parallelism. The variant 
      tested here (Jacobi preconditioner and CG solver) is fast and robust. For heavy use it is worth testing other combinations for you.

      lis needs the matrix storage sparseNonSym.

      the input can be generated by create_mesh.py --type bulk3d --res 4

      Verified in the sense, that the result matches a direct solver's result.
    </description>
  </documentation>

  <fileFormats>
    <input>
      <hdf5 fileName="lis.h5ref"/>
    </input>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="3d">
    <regionList>
      <region material="99lines" name="mech" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="3d">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
           <fix name="left"> 
              <comp dof="x"/> 
              <comp dof="y"/> 
              <comp dof="z"/>
           </fix>
           <force name="right">
             <comp dof="y" value="-1"/>
           </force>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <regionResult type="mechDeformEnergy">
             <allRegions/>
          </regionResult>
        </storeResults>
      </mechanic>
    </pdeList>

   <linearSystems>
     <system>
       <solutionStrategy>
         <standard>
           <!-- lis requires sparseNonSym storage  -->
           <matrix storage="sparseNonSym"/>
         </standard>
       </solutionStrategy> 
       <solverList>
         <!-- the combination jacobi and CG is fast and robust. For well conditioned problems other combinations can be faster. -->
         <lis>
           <precond>
             <jacobi/>
           </precond>
           <solver>
             <CG/>
          </solver>
         </lis>
        </solverList>
      </system>
    </linearSystems> 
  </sequenceStep> 
</cfsSimulation>


