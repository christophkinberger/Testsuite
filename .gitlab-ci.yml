# This file is used to define the CI pipeline for GitLab
# https://docs.gitlab.com/ee/ci/yaml

# https://docs.gitlab.com/ee/ci/variables/#variables-expressions
variables:
  GIT_STRATEGY: fetch # this is faster than clone and should re-use a workspace

# determine when pipelines are created
# https://docs.gitlab.com/ee/ci/yaml/#workflowrules
workflow:   
  rules:     
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# define stages (jobs in each stage run in parallel)
# https://docs.gitlab.com/ee/ci/yaml/#workflowrules
stages:
  - configure
  - stable
  - all


### CONFIGURE jobs ###
default:
  tags:
    - linux
  # global before-script setting up CFS and cmake
  before_script:
    - mkdir -p build
    - cd build
    # download latest CFS master build
    - wget -q https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz
    # install CFS by extracting
    - tar -xzvf CFS-master-Linux.tar.gz --strip-components=1
    # optionally install cmake if not present
    - cmake --version || ls -l cmake || ( wget https://cmake.org/files/v3.10/cmake-3.10.3-Linux-x86_64.sh && mkdir $(pwd)/cmake && sh cmake-3.10.3-Linux-x86_64.sh --prefix=$(pwd)/cmake --skip-license)
    - cmake --version || export PATH=$(pwd)/cmake/bin:$PATH
    - echo $PATH
    - ctest --version
    - cmake --version
  image: opencfs/ubuntu20.04 
  # cache cmake on runners
  cache:
    key: cmake
    paths:
      - build/cmake

configure:
  stage: configure
  script:
    # configure
    - cmake -DCFS_INSTALL_DIR=$(pwd) ..
    # show all tests
    - ctest -N

### TEST jobs ###
stable:
  stage: stable
  parallel:
    matrix:
      - TYPE: Singlefield
        PDE: [(Flow)|(Darwin),(Acou)|(WaterWaves),(Mechanics)|(Smooth),Magnetics,(Elec)|(Heat)]
      - TYPE: Coupledfield
        PDE: [.]
      - TYPE: Optimization
        PDE: [.]
      - TYPE: "(Solver)|(PYTHON)"
        PDE: [.]
  script:
    # configure
    - cmake -DCFS_INSTALL_DIR=$(pwd) -DTESTSUITE_THREADS=1 ..
    # test
    - echo "(${TYPE})_(${PDE})"
    - ctest --output-on-failure -R "($TYPE)_($PDE)" -LE unstable
  after_script:
    - ctest --output-on-failure --rerun-failed --repeat until-pass 5
  # https://docs.gitlab.com/ee/ci/yaml/#coverage
  # in the format '/REGEX/' - one can test REGEX at http://rubular.com/
  coverage: '/(\d+\%)\s+tests\s+passed,\s+\d+\s+tests?\s+failed\s+out\s+of\s+\d+/'

all:
  extends: stable
  stage: all
  script:
    # configure
    - cmake -DCFS_INSTALL_DIR=$(pwd) ..
    # test
    - echo "(${TYPE})_(${PDE})"
    - ctest --output-on-failure -R "(${TYPE})_(${PDE})"
  allow_failure: true
